import random
import numpy as np


class Mutation:

    def __init__(self, mutation_rate):
        self.MUTATION_RATE = mutation_rate
        pass

    def nonuniform_mutation(self, population: np.ndarray, dist='cauchy') -> np.ndarray:
        """
        Non-normal mutation
            1. Gaussian distribution
            2. Cauchy distribution
        """
        if dist == "cauchy":
            sample_function = np.random.standard_cauchy
        elif dist == "gaussian":
            sample_function = np.random.normal
        else:
            print("Wrong dist parameters")
        for f in range(0, len(population)):
            for i in range(0, len(population[f])):
                if np.random.uniform(0, 1) <= self.MUTATION_RATE:
                    noise = sample_function()
                    population[f][i] = population[f][i] + noise

        return population
