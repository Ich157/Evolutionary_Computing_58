import os.path
import sys
import argparse

sys.path.insert(0, 'evoman')

import numpy as np
from Selection import Selection
from Mutation import Mutation
from Crossover import Crossover
from Helpers import Helpers
from Statistics import Statistics

from demo_controller import player_controller
from environment import Environment
from typing import Tuple

parser = argparse.ArgumentParser()
parser.add_argument('-c', '--crossover', default='n-point', required=False, choices=['uniform', 'n-point'])
parser.add_argument('-n', '--n_points', default=5, required=False)
parser.add_argument('-e', '--enemies', default='1', required=False)
args = parser.parse_args()

CROSSOVER_TYPE = str(args.crossover)
N_POINTS = int(args.n_points)
ENEMY = [int(en) for en in args.enemies]

MULTIPLEMODE = 'no'
if len(ENEMY) > 1:
    MULTIPLEMODE = 'yes'
N_HIDDEN_NEURONS = 10
HEADLESS = True
KEEPWINNERS = 0.2
EXPERIMENT_NAME = 'enemy_' + str(ENEMY) + '_' + str(N_POINTS)
DEST_DIR = 'statistics/' + EXPERIMENT_NAME
ATTEMPTS = 10

# Allows the simulation to be run in the background, real slow and bothersome if turned off
if HEADLESS:
    os.environ["SDL_VIDEODRIVER"] = "dummy"

# Creates new directory for logs and results
if not os.path.exists(DEST_DIR):
    os.makedirs(DEST_DIR)

# Instantiate the environment
env = Environment(experiment_name=EXPERIMENT_NAME,
                  enemies=ENEMY,
                  playermode="ai",
                  player_controller=player_controller(N_HIDDEN_NEURONS),
                  enemymode="static",
                  level=2,
                  logs='off',
                  speed="fastest",
                  multiplemode=MULTIPLEMODE)

helper = Helpers(env)
N_POP = 100  # How many individuals per populations
N_GENERATIONS = 50  # How many generations the simulations goes through
N_WEIGHTS = (env.get_num_sensors() + 1) * N_HIDDEN_NEURONS + (
        N_HIDDEN_NEURONS + 1) * 5  # How many weights given the number of neurons
MUTATION_RATE = 0.2


def generate_first_population() -> Tuple[np.ndarray, np.ndarray]:
    """
    Generates the initial generation
    :return: The intial generation and
    """

    # Generate the first population randomly
    population = np.random.uniform(1, -1, (N_POP, N_WEIGHTS))

    # Evaluates the population
    fitness_population = helper.evaluate(population)

    return population, fitness_population


def evolve_population(parents: np.ndarray, fitness_parents: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """
    Takes a generation and evolves it to create a new one
    :param parents: The generation to evolve
    :param fitness_parents: The fitness of the current generation
    :return: A new generation and its fitness
    """
    selector = Selection(KEEPWINNERS)
    mutator = Mutation(MUTATION_RATE)
    crossover = Crossover(N_WEIGHTS, parents, fitness_parents)

    # Create offsrpings
    if CROSSOVER_TYPE == 'uniform':
        offsprings = crossover.uniform_crossing()
    else:
        offsprings = crossover.n_point_crossover(N_POINTS)
    mutated_offsprings = mutator.nonuniform_mutation(offsprings)
    fitness_mutated_offsprings = helper.evaluate(mutated_offsprings)

    # Selection for offsprings and parents
    selected_offsprings, fitness_selected_offsprings = selector.select(mutated_offsprings, fitness_mutated_offsprings)
    selected_parents, fitness_selected_parents = selector.select(parents, fitness_parents)
    selected_population, fitness_selected_population = np.concatenate(
        (selected_parents, selected_offsprings)), np.concatenate(
        (fitness_selected_parents, fitness_selected_offsprings))

    return selected_population, fitness_selected_population


if __name__ == '__main__':

    print(
        f"\nStarting simulation with following parameters:\n\nCrossover type: {CROSSOVER_TYPE}\n\nN-Points: {N_POINTS if CROSSOVER_TYPE == 'n-point' else 'N/A'}\n\nEnemies: {ENEMY}")

    statistics = Statistics(DEST_DIR)
    for j in range(ATTEMPTS):

        print(f'\nStarting attempt {j + 1}\n\nSimulating generation: 0')

        pop, fit_pop = generate_first_population()
        statistics.check_solution(pop, fit_pop, str(j + 1), True)
        stats = statistics.generate_statistics(0, fit_pop, j + 1, KEEPWINNERS)
        print(stats)

        for i in range(N_GENERATIONS):

            print(f'\nOn attempt {j + 1}\n\nSimulating generation: {i + 1}')

            if i == 0:
                KEEPWINNERS = 0.2
            if i == 10:
                KEEPWINNERS = 0.4
            if i == 20:
                KEEPWINNERS = 0.6
            if i == 30:
                KEEPWINNERS = 0.8
            if i == 40:
                KEEPWINNERS = 1

            evolved_population, fitness_evolved_population = evolve_population(pop, fit_pop)
            pop, fit_pop = evolved_population, fitness_evolved_population
            statistics.check_solution(pop, fit_pop, str(j + 1))
            stats = statistics.generate_statistics(i + 1, fit_pop, j + 1, KEEPWINNERS)
            print(stats)
