import numpy as np
import csv

class Statistics:

    def __init__(self, dest_dir):
        self.DEST_DIR = dest_dir

    def check_solution(self, population: np.ndarray, fitness_population: np.ndarray, attempt: str,
                       first_gen: bool = False) -> bool:
        """
        Checks the best solution and updates the overall best solution if possible
        :param attempt: Which attempt the iterator is on
        :param population: Numpy array of the population being evaluated
        :param fitness_population: Numpy array of the fitness of the population being evaluated
        :param first_gen: Boolean value to detect whether the solution being checked comes from the first generation
        :return: None
        """

        idx_best_solution = np.argmax(fitness_population)
        dna_best_individual = population[idx_best_solution]

        # If checking the first generation, simply save the best solution and indivdual as baseline
        if first_gen:

            np.savetxt(self.DEST_DIR + '/best_individual_attempt_' + attempt + '.txt', dna_best_individual)

            with open(self.DEST_DIR + '/best_solution_attempt_' + attempt + '.txt', 'w') as best_solution_file:
                best_solution_file.write(str(fitness_population[idx_best_solution]))

        # Otherwise check old and new solutions and update as required
        else:
            with open(self.DEST_DIR + '/best_solution_attempt_' + attempt + '.txt', 'r') as best_solution_file:
                old_solution = float(best_solution_file.readline())
                new_solution = fitness_population[idx_best_solution]

            if new_solution > old_solution:
                np.savetxt(self.DEST_DIR + '/best_individual_attempt_' + attempt + '.txt', dna_best_individual)

                with open(self.DEST_DIR + '/best_solution_attempt_' + attempt + '.txt', 'w') as best_solution_file:
                    best_solution_file.write(str(new_solution))

    def generate_statistics(self, generation: int, fitness_population: np.ndarray, attempt: int, keepwinner: float) -> str:

        best = fitness_population[np.argmax(fitness_population)]
        mean = np.mean(fitness_population)
        median = np.median(fitness_population)
        standard_deviation = np.std(fitness_population)

        if generation == 0 and attempt == 1:

            with open(self.DEST_DIR + '/results.csv', 'w', newline='') as file:
                writer = csv.writer(file, delimiter=',', dialect='excel')
                writer.writerow(
                    ['Attempt', 'KEEPWINNER', 'Generation', 'Best fitness', 'Mean fitness', 'Median fitness',
                     'Standard deviation'])
                writer.writerow([attempt, keepwinner, generation, best, mean, median, standard_deviation])

        else:
            with open(self.DEST_DIR + '/results.csv', 'a', newline='') as file:
                writer = csv.writer(file, delimiter=',', dialect='excel')
                writer.writerow([attempt, keepwinner, generation, best, mean, median, standard_deviation])

        return f'\nStatistics for generation: {generation}\n\nBest fitness: {best}\n\nFitness mean: {mean}\n\nFitness median: {median}\n\nFitness SD: {standard_deviation}'
