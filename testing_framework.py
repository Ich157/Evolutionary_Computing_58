import os
import sys

sys.path.insert(0, 'evoman')
from demo_controller import player_controller
from environment import Environment
from numpy import loadtxt
import csv
import numpy as np


N_HIDDEN_NEURONS = 10  # Don't change this value
HEADLESS = True

if HEADLESS:
    os.environ["SDL_VIDEODRIVER"] = "dummy"


# Checks that a file called 'test' with a .txt exists as that's what the environment uses to instantiate without a given experiment name
if not os.path.exists('test/evoman_logs.txt'):

    if not os.path.exists('test'):
        os.makedirs('test')

    with open('test/evoman_logs.txt', 'w') as f:
        f.write('PLACEHOLDER')

# initializes environment with ai player using random controller, playing against static enemy
env = Environment(playermode="ai",
                  player_controller=player_controller(N_HIDDEN_NEURONS),
                  enemymode="static",
                  level=2,
                  logs='off',
                  speed="fastest")


def simulate(environment: Environment, x: np.ndarray) -> int:
    """
    Simulates one round of player vs. enemy
    :param environment: The environment instantiated for the round
    :param x: The topology for the player controller
    :return:  individual gains for the simulated individual
    """

    fit, energy_player, energy_enemy, timesteps = environment.play(pcont=x)

    return energy_player - energy_enemy


for agent in os.listdir('statistics'):

    if str(agent) != 'enemy_[7, 8]_5' or str(agent) == 'results':
        continue

    EXPERIMENT_NAME = 'testing_' + str(agent)
    OUT_DIR = 'statistics/results/' + EXPERIMENT_NAME
    IN_DIR = 'statistics/' + str(agent)
    ENEMY = agent[6:-2]
    N_POINTS = agent[-1]

    print(f'\nAgent:\n\n\tEnemy: {ENEMY}\n\n\tN-point: {N_POINTS}')

    if not os.path.exists(OUT_DIR):
        os.makedirs(OUT_DIR)

    env.update_parameter('experiment_name', EXPERIMENT_NAME)

    for en in range(1, 9):

        print(f'\n\t\tEnemy: {en}')

        env.update_parameter('enemies', [en])

        for att in range(1, 11):

            print(f'\n\t\t\tAttempt: {att}')

            sol = loadtxt(IN_DIR + '/best_individual_attempt_' + str(att) + '.txt')

            ind_gain = 0

            for count in range(5):

                print(f'\n\t\t\t\tRound: {count + 1}')

                ind_gain += simulate(env, sol)

            ind_gain /= 5

            if en == 1 and att == 1:

                with open(OUT_DIR + '/results_' + str(EXPERIMENT_NAME) + '.csv', 'w', newline='') as f:
                    writer = csv.writer(f, delimiter=',', dialect='excel')
                    writer.writerow(['Enemy', 'Attempt', 'Individual gains'])
                    writer.writerow([en, att, ind_gain])

            else:
                with open(OUT_DIR + '/results_' + str(EXPERIMENT_NAME) + '.csv', 'a', newline='') as f:
                    writer = csv.writer(f, delimiter=',', dialect='excel')
                    writer.writerow([en, att, ind_gain])
