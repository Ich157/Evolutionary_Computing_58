import os.path
import random
import sys
import csv

import numpy as np

sys.path.insert(0, 'evoman')
from demo_controller import player_controller
from environment import Environment
from typing import Tuple

ENEMY = 1
N_HIDDEN_NEURONS = 10
HEADLESS = True
KEEPWINNERS = 1
EXPERIMENT_NAME = 'selfadaptive_'+'enemy_' + str(ENEMY) + '_' + str(KEEPWINNERS)
ATTEMPTS = 10

# Allows the simulation to be run in the background, real slow and bothersome if turned off
if HEADLESS:
    os.environ["SDL_VIDEODRIVER"] = "dummy"

# Creates new directory for logs and results
if not os.path.exists(EXPERIMENT_NAME):
    os.makedirs(EXPERIMENT_NAME)

# Instantiate the environment
env = Environment(experiment_name=EXPERIMENT_NAME,
                  enemies=[ENEMY],
                  playermode="ai",
                  player_controller=player_controller(N_HIDDEN_NEURONS),
                  enemymode="static",
                  level=2,
                  logs='on',
                  speed="fastest")

N_POP = 100  # How many individuals per populations
N_GENERATIONS = 30  # How many generations the simulations goes through
N_WEIGHTS = (env.get_num_sensors() + 1) * N_HIDDEN_NEURONS + (
        N_HIDDEN_NEURONS + 1) * 5  # How many weights given the number of neurons
MUTATION_RATE = 0.2


def simulate(environment: Environment, x: np.ndarray) -> float:
    """
    Simulates one round of player vs. enemy
    :param environment: The environment instantiated for the round
    :param x: The topology for the player controller
    :return: The fitness value for the simulated individual
    """

    fit, energy_player, energy_enemy, timesteps = environment.play(pcont=x)
    return fit


def evaluate(population: np.ndarray) -> np.ndarray:
    """
    Evaluates a given population by running a simulation for each individual
    :param population: A numpy array of the population to evaluate
    :return: A numpy array of fitness values for each individual
    """

    return np.array(list(map(lambda y: simulate(env, y[:-1]), population)))


def crossover(population: np.ndarray, fitness_population: np.ndarray) -> np.ndarray:
    """
    Crossover function
    :param population: The array of population
    :return: An array filled with offsprings
    """
    n_offspring = population.shape[0]  # Find the number of required offsprings

    parent1, parent2 = population[fitness_population.argsort()[-1]], population[fitness_population.argsort()[-2]]  # Choose best two parents

    off = np.zeros((n_offspring, N_WEIGHTS+1))  # Declare an array to be filled with the offprings

    for offspring in range(n_offspring):
        crossover_parameter = random.random()  # Choose a random parameter (between 0 and 1)

        off[offspring] = crossover_parameter * parent1 + (
                1 - crossover_parameter) * parent2  # Generate offsprings

    return off


def nonuniform_mutation(population: np.ndarray, dist='cauchy') -> np.ndarray:
    """
    Non-normal mutation
        1. Gaussian distribution
        2. Cauchy distribution
    """
    for f in range(0, len(population)):
        for i in range(0, len(population[f])-1):
            if np.random.uniform(0, 1) <= MUTATION_RATE:
                noise = population[f][-1]
                population[f][i] = population[f][i] + noise

    return population


def selection(population: np.ndarray, fitness_population: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """
    Selects individuals from a given population and its fitness
    :param population: The population from which to select individuals
    :param fitness_population: The fitness of the population
    :return: The selected population
    """

    y, x = np.shape(population)
    winners = np.empty([0, x])
    loosers = np.empty([0, x])
    winners_fitness = np.empty(0)
    loosers_fitness = np.empty(0)
    new_pop = np.empty([0, x])
    new_fitness = np.empty(0)

    # create numbers as long as the pop
    list = np.array(range(0, y))

    # get a random order
    random.shuffle(list)

    # select winners and loosers
    for i in range(0, y, 2):
        if fitness_population[list[i]] >= fitness_population[list[i + 1]]:
            winners = np.vstack((winners, population[list[i]]))
            winners_fitness = np.append(winners_fitness, fitness_population[list[i]])
            loosers = np.vstack((loosers, population[list[i + 1]]))
            loosers_fitness = np.append(loosers_fitness, fitness_population[list[i + 1]])
        else:
            loosers = np.vstack((loosers, population[list[i]]))
            loosers_fitness = np.append(loosers_fitness, fitness_population[list[i]])
            winners = np.vstack((winners, population[list[i + 1]]))
            winners_fitness = np.append(winners_fitness, fitness_population[list[i + 1]])

    # keep 80% of winners and 20% of loosers
    for i in range(0, int(y / 2), 1):
        if i < (y * KEEPWINNERS) / 2:
            new_pop = np.vstack((new_pop, winners[i]))
            new_fitness = np.append(new_fitness, winners_fitness[i])
        else:
            new_pop = np.vstack((new_pop, loosers[i]))
            new_fitness = np.append(new_fitness, loosers_fitness[i])

    return new_pop, new_fitness


def check_solution(population: np.ndarray, fitness_population: np.ndarray, attempt: str, first_gen: bool = False) -> bool:
    """
    Checks the best solution and updates the overall best solution if possible
    :param attempt: Which attempt the iterator is on
    :param population: Numpy array of the population being evaluated
    :param fitness_population: Numpy array of the fitness of the population being evaluated
    :param first_gen: Boolean value to detect whether the solution being checked comes from the first generation
    :return: None
    """

    idx_best_solution = np.argmax(fitness_population)
    dna_best_individual = population[idx_best_solution]

    # If checking the first generation, simply save the best solution and indivdual as baseline
    if first_gen:

        np.savetxt(EXPERIMENT_NAME + '/best_individual_attempt_' + attempt + '.txt', dna_best_individual)

        with open(EXPERIMENT_NAME + '/best_solution_attempt_' + attempt + '.txt', 'w') as best_solution_file:
            best_solution_file.write(str(fitness_population[idx_best_solution]))

    # Otherwise check old and new solutions and update as required
    else:
        with open(EXPERIMENT_NAME + '/best_solution_attempt_' + attempt + '.txt', 'r') as best_solution_file:
            old_solution = float(best_solution_file.readline())
            new_solution = fitness_population[idx_best_solution]

        if new_solution > old_solution:
            np.savetxt(EXPERIMENT_NAME + '/best_individual_attempt_' + attempt + '.txt', dna_best_individual)

            with open(EXPERIMENT_NAME + '/best_solution_attempt_' + attempt + '.txt', 'w') as best_solution_file:
                best_solution_file.write(str(new_solution))

            return True
        else:
            return False


def generate_statistics(generation: int, fitness_population: np.ndarray, attempt: int) -> str:

    best = fitness_population[np.argmax(fitness_population)]
    mean = np.mean(fitness_population)
    median = np.median(fitness_population)
    standard_deviation = np.std(fitness_population)

    if generation == 0 and attempt == 1:

        with open(EXPERIMENT_NAME + '/statistics_' + str(EXPERIMENT_NAME) + '.csv', 'w', newline='') as file:
            writer = csv.writer(file, delimiter=',', dialect='excel')
            writer.writerow(['Attempt', 'Generation', 'Best fitness', 'Mean fitness', 'Median fitness', 'Standard deviation'])
            writer.writerow([attempt, generation, best, mean, median, standard_deviation])

    else:
        with open(EXPERIMENT_NAME + '/statistics_' + str(EXPERIMENT_NAME) + '.csv', 'a', newline='') as file:
            writer = csv.writer(file, delimiter=',', dialect='excel')
            writer.writerow([attempt, generation, best, mean, median, standard_deviation])

    return f'\nStatistics for generation: {generation}\n\nBest fitness: {best}\n\nFitness mean: {mean}\n\nFitness median: {median}\n\nFitness SD: {standard_deviation}'


def generate_first_population() -> Tuple[np.ndarray, np.ndarray]:
    """
    Generates the initial generation
    :return: The intial generation and
    """

    # Generate the first population randomly
    population = np.random.uniform(1, -1, (N_POP, N_WEIGHTS+1))

    # Evaluates the population
    fitness_population = evaluate(population)

    return population, fitness_population


def evolve_population(parents: np.ndarray, fitness_parents: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """
    Takes a generation and evolves it to create a new one
    :param parents: The generation to evolve
    :param fitness_parents: The fitness of the current generation
    :return: A new generation and its fitness
    """

    # Create offsrpings
    offsprings = crossover(parents, fitness_parents)
    mutated_offsprings = nonuniform_mutation(offsprings)
    fitness_mutated_offsprings = evaluate(mutated_offsprings)

    # Selection for offsprings and parents
    selected_offsprings, fitness_selected_offsprings = selection(mutated_offsprings, fitness_mutated_offsprings)
    selected_parents, fitness_selected_parents = selection(parents, fitness_parents)
    selected_population, fitness_selected_population = np.concatenate((selected_parents, selected_offsprings)), np.concatenate((fitness_selected_parents, fitness_selected_offsprings))

    return selected_population, fitness_selected_population


if __name__ == '__main__':

    for j in range(ATTEMPTS):

        print(f'\nStarting attempt {j + 1}\n\nSimulating generation: 0')

        pop, fit_pop = generate_first_population()
        check_solution(pop, fit_pop, str(j + 1), first_gen=True)
        stats = generate_statistics(0, fit_pop, j + 1)
        print(stats)

        for i in range(N_GENERATIONS):

            print(f'\nSimulating generation: {i + 1}')

            evolved_population, fitness_evolved_population = evolve_population(pop, fit_pop)
            pop, fit_pop = evolved_population, fitness_evolved_population
            check_solution(pop, fit_pop, str(j + 1))
            stats = generate_statistics(i + 1, fit_pop, j + 1)
            print(stats)
