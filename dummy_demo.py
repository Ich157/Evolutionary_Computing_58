################################
# EvoMan FrameWork - V1.0 2016 #
# Author: Karine Miras         #
# karine.smiras@gmail.com      #
################################

# imports framework
import os
import sys

sys.path.insert(0, 'evoman')
from demo_controller import player_controller
from environment import Environment
from numpy import loadtxt

# Modify the next three gobal parameters to test a specific agent
ENEMY = 2
KEEPWINNERS = 1
ATTEMPT = 6

EXPERIMENT_NAME = 'enemy_' + str(ENEMY) + '_' + str(KEEPWINNERS)

if not os.path.exists(EXPERIMENT_NAME):
    os.makedirs(EXPERIMENT_NAME)

N_HIDDEN_NEURONS = 10  # Don't change this value


# initializes environment with ai player using random controller, playing against static enemy
env = Environment(experiment_name=EXPERIMENT_NAME,
                  enemies=[ENEMY],
                  playermode="ai",
                  player_controller=player_controller(N_HIDDEN_NEURONS),
                  enemymode="static",
                  level=2,
                  speed="fastest")

sol = loadtxt(EXPERIMENT_NAME + '/best_individual_attempt_' + str(ATTEMPT) + '.txt')

env.play(sol)

