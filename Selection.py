import numpy as np
import random
from typing import Tuple


class Selection:

    def __init__(self, KEEPWINNERS):
        self.KEEPWINNERS = KEEPWINNERS


    def select(self, population: np.ndarray, fitness_population: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        """
        Selects individuals from a given population and its fitness
        :param population: The population from which to select individuals
        :param fitness_population: The fitness of the population
        :return: The selected population
        """

        y, x = np.shape(population)
        winners = np.empty([0, x])
        loosers = np.empty([0, x])
        winners_fitness = np.empty(0)
        loosers_fitness = np.empty(0)
        new_pop = np.empty([0, x])
        new_fitness = np.empty(0)

        # create numbers as long as the pop
        list = np.array(range(0, y))

        # get a random order
        random.shuffle(list)

        # select winners and loosers
        for i in range(0, y, 2):
            if fitness_population[list[i]] >= fitness_population[list[i + 1]]:
                winners = np.vstack((winners, population[list[i]]))
                winners_fitness = np.append(winners_fitness, fitness_population[list[i]])
                loosers = np.vstack((loosers, population[list[i + 1]]))
                loosers_fitness = np.append(loosers_fitness, fitness_population[list[i + 1]])
            else:
                loosers = np.vstack((loosers, population[list[i]]))
                loosers_fitness = np.append(loosers_fitness, fitness_population[list[i]])
                winners = np.vstack((winners, population[list[i + 1]]))
                winners_fitness = np.append(winners_fitness, fitness_population[list[i + 1]])

        # keep 80% of winners and 20% of loosers
        for i in range(0, int(y / 2), 1):
            if i < (y * self.KEEPWINNERS) / 2:
                new_pop = np.vstack((new_pop, winners[i]))
                new_fitness = np.append(new_fitness, winners_fitness[i])
            else:
                new_pop = np.vstack((new_pop, loosers[i]))
                new_fitness = np.append(new_fitness, loosers_fitness[i])

        return new_pop, new_fitness