import numpy as np
import random
from typing import Tuple


class Crossover:

    def __init__(self, n_weights: int, population: np.ndarray, fitness_population: np.ndarray):
        self.n_weights = n_weights
        self.population = population
        self.fitness_population = fitness_population
        self.n_alleles = self.population.shape[1]
        self.n_offsprings = self.population.shape[0]
        self.offsprings = np.zeros((self.n_offsprings, self.n_weights))

    def choose_parents(self) -> Tuple[np.ndarray, np.ndarray]:
        """
        Selects the best two parents for crossover
        :return: A tuple of arrays representing the top two parents
        """
        return self.population[self.fitness_population.argsort()[-1]], self.population[
            self.fitness_population.argsort()[-2]]

    def find_points(self, n: int) -> list:
        """
        Determines where to split the genomes for the n-point crossover
        :param n: The count of splits
        :return: A list of indexes for each split
        """
        points = [0]
        floor = 0
        r = 0

        for count in range(n):
            while r == points[-1]:  # Checks that the split point is not zero or the same as the previously generated one
                r = random.randrange(floor, self.n_alleles - (
                            n - count + 1))  # Guarantees there is enough room for the count of splits

            floor = r

            points.append(r)

        # Append the size of the array to make sure the last split is performed in n_point_crossover
        points.append(self.n_alleles)

        return points

    def n_point_crossover(self, n_points: int) -> np.ndarray:
        """
        Performs an n-point crossover
        :param n_points: How many splits to perform
        :return: An array of offsprings
        """
        parent1, parent2 = self.choose_parents()

        for offspring in range(0, self.n_offsprings // 2, 2):
            points = self.find_points(n_points)
            r = points[0]

            for point in points[1:]:
                if points.index(point) % 2 == 0:  # Checks whether to switch the alleles or not
                    self.offsprings[offspring][r:point], self.offsprings[offspring + 1][r:point] = parent1[r:point], parent2[r:point]
                else:
                    self.offsprings[offspring][r:point], self.offsprings[offspring + 1][r:point] = parent2[r:point], parent1[r:point]
                r = point

        return self.offsprings

    def uniform_crossing(self) -> np.ndarray:
        """
        Uniform crossover function
        :return: An array filled with offsprings
        """
        parent1, parent2 = self.choose_parents()

        for offspring in range(self.n_offsprings):
            crossover_parameter = random.random()  # Choose a random parameter (between 0 and 1)

            self.offsprings[offspring] = crossover_parameter * parent1 + (
                        1 - crossover_parameter) * parent2  # Generate offsprings

        return self.offsprings
