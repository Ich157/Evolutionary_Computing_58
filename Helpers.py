from environment import Environment
import numpy as np


def simulate(environment: Environment, x: np.ndarray) -> float:
    """
    Simulates one round of player vs. enemy
    :param environment: The environment instantiated for the round
    :param x: The topology for the player controller
    :return: The fitness value for the simulated individual
    """

    fit, energy_player, energy_enemy, timesteps = environment.play(pcont=x)
    return fit


class Helpers:

    def __init__(self, env):
        self.env = env

    def evaluate(self, population: np.ndarray) -> np.ndarray:
        """
        Evaluates a given population by running a simulation for each individual
        :param population: A numpy array of the population to evaluate
        :return: A numpy array of fitness values for each individual
        """

        return np.array(list(map(lambda y: simulate(self.env, y), population)))
